Simple Web App Example
======================

The folder `whatstheweather` contains source code of a simple Python web
application that can be deployed on Google App Engine. 

A version of the application is currently deployed at 
[carbon-compound-433.appspot.com](https://carbon-compound-433.appspot.com).

If you deploy to your own GAE project, you must change the application id
in `app.yaml`.
