import webapp2
import jinja2
import os.path
import engine
from time import asctime, localtime

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class MainPage(webapp2.RequestHandler):

    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        template = JINJA_ENVIRONMENT.get_template('files/templates/main.html')
        data = {'timestamp': asctime(localtime())}
        self.response.write(template.render(data))

application = webapp2.WSGIApplication([
    ('/api/.*', engine.Api),
    ('/', MainPage),
], debug=True)
