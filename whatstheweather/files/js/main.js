var validities = ['empty', 'incomplete', 'illegal', 'valid', 'unknown', 'known'];
var messages = new Array();
messages['empty'] = '';
messages['incomplete'] = 'Not enough characters in zip code';
messages['illegal'] = 'Illegal characters in zip code';
messages['valid'] = '';
messages['unknown'] = 'Unknown location';
messages['known'] = '';
var zipValidity = 'empty';
var lastZip = '';

function fetchWeather() {
    var zipcode = $('input#zip').val();
    console.log("fetching weather...");
    $.ajax('/api/weather?zip=' + zipcode, {
        beforeSend: function() {
            $("img#spinner").css({'visibility': 'visible'});
        },
        success: function(data) {
            $('div#weather-result-container').text(data.weather);
        },
        error: function() {
            console.log("error fetching weather");
        },
        complete: function() {
            $("img#spinner").css({'visibility': 'hidden'});
        },
        dataType: 'json'
    });
    return false;
}

function checkValidity(value) {
    if (value.length == 0) {
        return 'empty';
    } else if (value.match(/^\d+$/)) {
        return value.length < 5 ? 'incomplete' : 'valid';
    } else {
        return 'illegal';
    }
}

function locationFetched(data) {
    console.log(data);
    zipValidity = data.known ? 'known' : 'unknown';
    zipValidityChanged();
    if (data.known) {
        $('div#location-result-container').text(data.city + ", " + data.state);
    }
}

function zipValidityChanged() {
    var container = $('div#location-result-container');
    container.addClass(zipValidity);
    validities.forEach(function(validity){
        if (validity != zipValidity) {
            container.removeClass(validity);
        }
    });
    container.text(messages[zipValidity]);
    $('input#check').attr('disabled', zipValidity != 'known');
}

function zipInputChanged() {
    $('div#weather-result-container').text('');
    var value = $('input#zip').val();
    if (value == lastZip) {
        return false;
    }
    lastZip = value;
    console.log("zipInputChanged: " + value);
    var oldValidity = zipValidity;
    zipValidity = checkValidity(value);
    if (zipValidity != oldValidity) {
        zipValidityChanged();
    }
    if (zipValidity == 'valid') {
        $.ajax('/api/location?zip=' + value, {
            success: function(data) {
                locationFetched(data);
            }, 
            error: function() {
                console.log("error fetching location");
            },
            dataType: 'json'
        });
    }
    return false;
}    

$(document).ready(function(){
    $('input#zip').on("change paste keyup", function(){ 
        return zipInputChanged();
    });
});
    
