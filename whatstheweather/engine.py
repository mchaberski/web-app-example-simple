import webapp2
import json
import datalayer

class Api(webapp2.RequestHandler):

    def get(self):
        if self.request.path == '/api/location':
            self.get_location()
        elif self.request.path == '/api/weather':
            self.get_weather()
        else:
            self.response.status = 404
            
    def get_location(self):
        zipcode = self.request.params['zip']
        if zipcode is None:
            self.response.status = 400
        else:
            self.response.headers['Content-Type'] = 'text/plain'
            city, state = datalayer.fetch_location(zipcode)
            response_content = {
                'zip': zipcode,
                'known': city is not None,
                'city': city, 
                'state': state
            }
            self.response.write(json.dumps(response_content))
    
    def get_weather(self):
        zipcode = self.request.params['zip']
        if zipcode is None:
            self.response.status = 400
        else:
            self.response.headers['Content-Type'] = 'text/plain'
            city, state = datalayer.fetch_location(zipcode)
            weather = datalayer.fetch_weather(zipcode)
            response_content = {
                'zip': zipcode,
                'known': city is not None,
                'city': city, 
                'state': state,
                'weather': weather if city is not None else None,
            }
            self.response.write(json.dumps(response_content))
        
